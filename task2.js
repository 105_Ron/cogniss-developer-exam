const jsonfile = require('jsonfile');
const randomstring = require('randomstring');
const got = require('got');

const acronyms = [...Array(10)].map(() => randomstring.generate(3).toUpperCase());
const inputURL = 'http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=';
const outputFile = 'output3.json';

console.log('calling looping function');

(async function getAcronym() {
  Promise.all(
    acronyms.map(async (acronym) => {
      let definitions = null;
      const { err, body: responseBody } = await got(`${inputURL}${acronym}`);
      if (err) {
        console.log('error', err);
      } else {
        const [body] = JSON.parse(responseBody);
        definitions = body?.lfs.map(({ lf }) => lf) || [];
        console.log(`${definitions ? 'got' : 'no'} data ${definitions ? '' : 'found'} for acronym ${acronym}`);
        definitions && console.log('add returned data to definitions array');
      }
      return { [acronym]: definitions };
    }),
  ).then((values) => {
    const output = values.reduce((accum, acronym) => (
      {
        ...accum,
        ...acronym,
      }
    ), {});
    console.log('saving output file formatted with 2 space indenting');
    jsonfile.writeFile(outputFile, output, { spaces: 2 }, () => {
      console.log('All done!');
    });
  }).catch((err) => console.log(err.message));
}());