const jsonfile = require('jsonfile');
const randomstring = require('randomstring');

const inputFile = 'input2.json';
const outputFile = 'output2.json';

console.log('loading input file...', inputFile);
jsonfile.readFile(inputFile, (err, body) => {
  console.log('Generating user emails...');
  const { names } = body;
  const emails = names.map((name) => (
    `${[...name].reverse().join('')}${randomstring.generate(5)}@gmail.com`));
  const output = { emails };
  console.log('Previewing output\n{\n  "emails":', emails, '\n}');
  console.log('saving output file formatted with 2 space indenting');
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, () => {
    console.log('All done!');
  });
});
